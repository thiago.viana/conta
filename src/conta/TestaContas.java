package conta;

class TestaContas {

	public static void main(String[] args) {

		ContaCorrente cc = new ContaCorrente();
		ContaPoupanca cp = new ContaPoupanca();

		cc.deposita(1000.0);
		cp.deposita(1000.0);

		cc.atualiza(0.1);
		cp.atualiza(0.1);

		System.out.println(cc.getSaldo());
		System.out.println(cp.getSaldo());
	}
}
