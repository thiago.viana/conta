package conta;

class AtualizadorDeContas {

	private double saldoTotal;
	private double selic;

	public AtualizadorDeContas(double selic) {
		this.selic = selic;
	}

	public void roda(Conta c) {
		System.out.println("===============================");
		System.out.println("Saldo Anterior: " + c.getSaldo());
		c.atualiza(this.selic);
		System.out.println("Saldo Atualizado: " + c.getSaldo());
		saldoTotal += c.getSaldo();
	}

	public double getSaldoTotal() {
		return this.saldoTotal;
	}
}